# Manufacturing

Sophio Japharidze, IVSM  
Mariam Mikava, IVSM  
Ketevani Kvirikashvili, IVSM  
Lekso Migriauli, IVSM  

Team manufacturing

# Running Tests

```
$ cd manufacturing-tdd
$ mix deps.get
$ mix test
```

Main files of concern:
* **test/manufacturing_test.exs** - tests and corresponding requirements are written here
* **lib/manufacturing.ex** - main functionality is written here