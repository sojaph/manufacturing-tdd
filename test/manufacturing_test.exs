defmodule ManufacturingTest do
  use ExUnit.Case
  doctest Manufacturing

  def data do
    startTime = Timex.to_datetime({{2019, 10, 24}, {9, 0, 0}}, "GMT+3")
    endTime = Timex.to_datetime({{2019, 10, 24}, {18, 30, 0}}, "GMT+3")
    %{startTime: startTime,
      endTime: endTime,
      plannedDowntime: 60,
      ict: 190,
      finishedProducts: 247,
      rejectedProducts: 14
    }
  end
  # requirement 1 - method for calculating manufacturing efficiency indicators exists
  test "Method for calculation exists" do
    assert Manufacturing.calculate(data())
  end

  #requirement 2 - method calculates planned production time (PPT). PPT = total shift duration - planned downtime
  test "Method calculates PPT" do
    assert %{PPT: 510} = Manufacturing.calculate(data())
  end

  #requirement 3 - method calculates Ideal Production Rate(IPR) when provided with Ideal Cycle Time(ICT)
  # IPR = 1 / ICT
  test "Method calculates IPR" do
    assert %{IPR: 0.00526} = Manufacturing.calculate(data())
  end

  # requirement 4 - method calculates Quality factor (Q). Q = (finished products / total products produced) * 100
  test "Method calculates Q" do
    assert %{Q: 94.6} = Manufacturing.calculate(data())
  end
end
