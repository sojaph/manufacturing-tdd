defmodule Manufacturing do
  def calculate(data) do
    # PPT = (shiftEndTime - shiftStartTime) - plannedDowntime [min]
    ppt = Timex.diff(data.endTime, data.startTime, :minutes) - data.plannedDowntime
    # IPR = 1 / ICT [units / sec]
    ipr = (1 / data.ict) |> Float.round(5)
    # Q = (finished products / total products produced) * 100
    q = (data.finishedProducts / (data.finishedProducts + data.rejectedProducts)) * 100 |> Float.round(1)

    %{PPT: ppt, IPR: ipr, Q: q}
  end
end
